# Event DRI section 

* [ ]  **Please link this request issue to the corresponding epic.**
* [ ]  **Please link all relevant docs, e.g. meeting and planning docs, to this issue.**

## Requested Deadline: `YYYY-MM-DD`

---
# Requestor Section

## Video Details
* Description: 
* Link: 
* Tags: 
* CTA
* Existing video:
* Slidedeck:
* Script: 
* **Campaign and/or event:** 
* **# of expected videos:** 
* **Budget:** 
* **Campaign tag:** 

## Checklist

- [ ] Video has GitLab branded video bumpers
- [ ] Video has been reviewed and approved for brand compliance 
- [ ] Video details have beed added to this issue.
- This video template is for requesting a completed, polished video to be uploaded to YouTube that does not need any additional editing. If you need video editing assistance, please open a `video-editing` template.  [video editing request template](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/digital-production/-/blob/master/.gitlab/issue_templates/video-editing-request.md)

## Required for posting

- [ ] I have permission from all parties involved to make this video public

### Promotion strategy 

<!-- Requestor to fill out in detail. Let us know what you're aiming to accomplish and how the final content will be shared and promoted. Briefly explain what this video is about and who the audience is-->

/assign @atflowers

/label ~"Digital production" ~"mktg-status::triage" ~"Video upload request" ~"mktg-growth" ~Events 